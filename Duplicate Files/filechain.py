import hashlib
import os
class filechain:
	def __init__ (self):
		self.filesizes = []
		self.chain={}
		self.duplicate={}
	
	#Inserts new file into dict based on its key value ie. file size
	def insert (self,path,size):
		self.chain.setdefault(size,[]).append(path)

	#Function to compare (hashed) contents of files among the groups.
	def compare_files(self):
		group_no=0
		for k,files in self.chain.items():
			if(len(files)>1):
				hash_store = {}
				#Store files in dict hash_store with key value as its hash value ie. filehash
				for f in files:
					filehash = hashlib.md5(open(f,'r').read()).hexdigest()
					hash_store.setdefault(filehash,[]).append(f)
				#If length of list in a particular value of hash_store > 1 ie. Duplicates present. Copy to duplicate dict with group no. key
				for lists in hash_store.values():
					if len(lists) > 1:
						self.duplicate.setdefault(group_no,lists)
						group_no = group_no + 1

	#Function to display duplicate groups
	def display(self):
		cost = 0
		for group_no,files in self.duplicate.items():
			print "Duplicate Group No. :" ,group_no
			for f in files:
				print files.index(f)+1,f
			print
			cost = cost + os.path.getsize(files[0]) * (len(files)-1)
		if cost == 0:
			print "No duplicate files found."
		else:
			print "Possible storage savings without loss of data :",cost,"bytes"
		print 

	#Function to provide option to delete duplicate files.
	def delete_option(self,remove):
		cost = 0
		yes = ['yes','YES','Yes','1','y','Y']
		no = ['no','NO','No','0','n','N']
		for group_no,files in self.duplicate.items():
			original = False
			print "Duplicate Group No. :" ,group_no
			for f in files:
				if remove == False:
					choice = raw_input("Do you want to delete file '"+f+"'? (Y/N): ")
				elif original == True:
					choice = 'Y'
				else:
					choice = 'N'
					original = True
				if choice in yes:
					cost = cost + os.path.getsize(f)
					print f+" deleted!"
					os.remove(f)
				elif not choice in no:
					print "Incorrect input, file not deleted!"
			print
		print "You freed",cost,"bytes"