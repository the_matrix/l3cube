Problem 5 :
Program to list duplicate files in a directory

Functionalities :
- Display list of duplicate files in groups.
- Display possible storage savings (in bytes) without loss of data.
- Remove all duplicate files in a directory retaining only one copy of file.
- Display list of duplicate files and ask user which files should be deleted.
- Display amount of storage saved after deletion of duplicate files.


Usage:
python run.py [-h] [-p PATH] [-r] [-d]

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  sets PATH as the root directory which has to be
                        searched for duplicate files. By default, this path is
                        set to './Files/'
  -r, --remove          deletes all duplicate files leaving only one file from
                        each duplicate group. By default, decision of deletion
                        of every file is asked from user.
  -d, --disp            only displays groups of duplicate files without option
                        of deletion

Example : 
python run.py -d 		displays groups of duplicate files in the default 
						directory './Files/'
python run.py -p ./../	displays groups of duplicate files in the parent
						of the current working directory.
python run.py -r 		deletes all duplicate files leaving only one file
						from each duplicate group.