import os.path
import filechain
import argparse
import sys

if __name__ == "__main__":
	
	root_path = "./Files/"
	example = 'Example : python run.py -d -p ./..'

	#Get options from user
	parser = argparse.ArgumentParser(epilog = example)
	parser.add_argument('-p','--path',nargs=1,help="sets PATH as the root directory which has to be searched for duplicate files. By default, this path is set to './Files/'")
	parser.add_argument('-r','--remove',help='deletes all duplicate files leaving only one file from each duplicate group. By default, decision of deletion of every file is asked from user.',action='store_true')
	parser.add_argument('-d','--disp',help='only displays groups of duplicate files without option of deletion',action='store_true')
	args = parser.parse_args()
	
	#Set root directory path
	if args.path is not None:
		root_path = args.path[0]
		if os.path.isdir(root_path) == False:
			print "Directory does not exists."
			sys.exit(1)
	remove_all_duplicates = args.remove
	print "Finding duplicate files in '"+root_path+"' :"
	
	f = filechain.filechain()
	
	#Find all files in the root directory and its subdirectories and store it in a dict with key value as file size.
	for path,directory,files in os.walk(root_path):
		for filename in files:
			root_path = os.path.join(path,filename)
			filesize = os.path.getsize(root_path)
			f.insert(root_path,filesize)
	f.compare_files()
	f.display()
	if args.disp is None:
		f.delete_option(remove_all_duplicates)