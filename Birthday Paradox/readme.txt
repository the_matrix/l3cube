Problem 1:
Verify Birthday Paradox

Summary:
The program verifies the birthday paradox for a set of random birthdays and a given number of such groups to 
find the probability that atleast two persons in a group have the same birthdays.

Functionality:
- Displays the calculated probability over random groups of randomized birthdays.
- Displays the exact probability of the same scenario.
- Displays the indices at which matching birthdays are found, if any.

Input:
- Number of persons. (N)
- Number of such groups over which to calculate the probability. (G)

Usage:
python birthday.py

Working:
The program generates G groups of N random birthdays and checks if matching birthdays are found in any group. The required probability is
found from these groups.
