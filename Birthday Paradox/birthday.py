from __future__ import division
import random
import find_birthday
import sys
#Generate N random numbers in the range 1-365 and checking if two people have the same birthday
def birthday(N):
	persons=[]
	for i in range(N):
		persons.append(random.randint(1,365))
	persons.sort()
	count = 1 
	for i in persons:
		date,month=find_birthday.find_birthday(i)
		print "Person",count,":",date,month
		count+=1
	found=0
	flag=0
	for i in range(len(persons)-1):
		if(persons[i] == persons[i+1]):
			found=1
			print
			print "Match found at index: ",(i+1),",",(i+2)
			flag=1
			break
	if flag==0:
		print "No match found"
	return found

#finding the exact probability
def exactProb(N):
	prob=1.0
	for i in range(N):
		prob*=(1.0-(i/365.0))
	return 1-prob

if __name__=="__main__":
	flags=[]
	count=0
	try:
		print "Enter the number of persons :"
		N=int(input()) #the number of persons
		print("Enter the number of groups :")
		R=int(raw_input()) # the number of groups (iterations) for the experiment
	except:
		print "Invalid number entered!"
		sys.exit(1)
	for i in range(R):
		print
		print "Group (Iteration) no : ",(i+1)
		print 
		found=birthday(N)
		flags.append(found)
		print "---------------xx---------------"
	for i in flags:
		if(i==1):
			count+=1
	prob=count/R 
	exProb=exactProb(N)
	print
	print "Number of times same birthdays found: ",count
	print "Exact probability: ",prob
	print "Expected probability: ",exProb
	print
			
	
		
