import statistics
import tokenize
if __name__ == "__main__":
	stat = statistics.statistics()	#initialize in constructor
	f = open('weblog.txt','r')
	for line in f:
		tokens = tokenize.tokenize(line)	#tokenize each line using tokenize function
		stat.update(tokens)
	stat.display()