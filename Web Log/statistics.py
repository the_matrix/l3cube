#Statistics class to handle the analyzing of log file
class statistics: 
	
	def __init__(self):
		self.remotehost=[]
		self.referral=[]
		self.file_requested=[]
		self.requested_number=[]
		self.agent=[]
		self.status=[]
		self.statusType=[]
		self.OS=[]
		self.OSType=[]
		
	#insert new entry into hosts list
	def insertInHost(self,new_entry):
		if new_entry in self.remotehost:
			self.remotehost.index(new_entry)
		else:
			self.remotehost.append(new_entry)
	
	#insert new entry into referrals list
	def insertInReferral(self,new_entry):
		if new_entry=='-':
			return
		if new_entry in self.referral:
			self.referral.index(new_entry)
		else:
			self.referral.append(new_entry)
	
	#insert new entry into requested files list
	def insertInFile_requested(self,new_entry):
		if new_entry in self.file_requested:
			found=self.file_requested.index(new_entry)
			self.requested_number[found]=self.requested_number[found]+1
		else:
			self.file_requested.append(new_entry)
			self.requested_number.append(1)
		
	#insert new entry into agents list
	def insertInAgent(self,new_entry):	
		if new_entry in self.remotehost:
			self.agent.index(new_entry)
		else:
			self.agent.append(new_entry)
				
	#updating various lists
	def update(self,tokens):	
		self.insertInHost(tokens.group(1))
		token=tokens.group(5)[4::]
		self.insertInFile_requested(token)
		self.insertInReferral(tokens.group(8))
		self.insertInAgent(tokens.group(9))
		self.insertInStatus(tokens.group(6))
		self.insertInOS(tokens.group(9))

	#insert new entry into OS list  
	def insertInOS(self,new_entry):
		add='Other'
		mac='Macintosh'
		win='Windows'
		lin='Linux'
		if mac in new_entry:
						add=mac                
		if win in new_entry:
						add=win                
		if lin in new_entry:
						add=lin
		if add in self.OS:
						found=self.OS.index(add)
						self.OSType[found]+=1
		else:
						self.OS.append(add)
						self.OSType.append(1)
		
	#insert new entry into status list
	def insertInStatus(self,new_entry):
		if new_entry in self.status:
			found=self.status.index(new_entry)
			self.statusType[found]+=1
		else:
			self.status.append(new_entry)
			self.statusType.append(1)		

	#displaying various details
	def display(self):
		
		print
		print "--------------------General Statistics---------------------"
		n=len(self.remotehost)
		print "Number of Unique Hosts :",n
		n1=len(self.file_requested)
		print 
		print "Total number of unique pages requested :",n1
		n2=max(self.requested_number)
		most_visited=self.requested_number.index(max(self.requested_number))
		print "Most frequently visited page :",self.file_requested[most_visited]
		print "Frequency of the most visited page : ",max(self.requested_number)
		
		print
		n3=len(self.referral)
		print "Number of Unique Referrers :",n3
		referer=self.referral.index(max(self.referral))
		print "Number of redirections from the above referrer : ",max(self.referral)
		
		print 
		n4=len(self.agent)
		print "Number of Unique Agents :",n4
		xx2=0
		xx3=0
		xx4=0
		xx5=0
		
		for i in range(len(self.status)):
						
						if(self.status[i][:1]=='2'):
								xx2+=self.statusType[i]
						if(self.status[i][:1]=='3'):
								xx3+=self.statusType[i]
						if(self.status[i][:1]=='4'):
								xx4+=self.statusType[i]
						if(self.status[i][:1]=='5'):
								xx5+=self.statusType[i]	
		print
		print "Successful responses (2XX):",xx2
		print "Redirections (3XX):",xx3
		print "Client Errors (4XX):",xx4
		print "Server Errors (5XX):",xx5

		print
		print "Operating System details :"
		for i in range(len(self.OS)):
						print "Number of",self.OS[i],"users: ",self.OSType[i]

		
		choice=1          
		while choice!=6:
						print "-----------------------------------------------------------"
						print "Enter choice for viewing detailed report :\n1. Unique Hosts\n2. Requested files\n3. Unique Referrals\n4. Unique Agents\n5. Exit"
						print
						print "Enter choice:",
						choice= raw_input()
						
						if choice=='1':
								print "Unique hosts : "
								for i in range(len(self.remotehost)):
										print i+1,":",self.remotehost[i]
						
						if choice=='2':
								print "Files requested along with their frequencies"
								for i in range(len(self.file_requested)):
										print i+1,":",self.file_requested[i],"-",self.requested_number[i]," time(s)"
						if choice=='3':
								for i in range(len(self.referral)):
										print i+1,":",self.referral[i]
						if choice=='4' :
								for i in range(len(self.agent)):
										print i+1,":",self.agent[i]
						if(choice=='5'):
								break
