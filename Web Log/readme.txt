Problem 4:
Making sense of a web log file.

Functionalities:
- Read and parse a web log file into various fields, viz., host, request, referrers, user agent etc.
- Display statistics:
	- Unique hosts.
	- Unique pages requested.
	- Most frequently visited page and its frequecy.
	- Unique referrers.
	- Web page giving maximum redirections.
	- Unique agents.
	- Number of Mac Os/ Windows/ Linux/ Other OS users.
	- Number of Successful responses.
	- Number of Redirections.
	- Number of Client Errors.
	- Number of Server Errors.
- Display the lists of unique hosts, requested files, unique referrals and agents.

Usage:
python run.py

Note:
The program reads and analyzes the 'weblog.txt' file present in the source folder.