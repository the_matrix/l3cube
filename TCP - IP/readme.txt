Problem 3:
PCAP File Analyzer

Functionalities :
- Extract a pcap file and display contents.
- Display details of pcap header.
- Display details of packet header.
- Display details of IPv4, ARP, TCP packets.
- Count total number of packets.

usage: 
python run.py [-h] pcap_file

positional arguments:
  pcap_file   path of the pcap file to be analyzed.

optional arguments:
  -h, --help  show this help message and exit

example:
python run.py ./arp-storm.pcap
python run.py ./tcp-ecn-sample.pcap
python run.py ./bgp.pcap
python run.py ./http_witp_jpegs.cap