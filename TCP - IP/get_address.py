import basic_byte_op

#Function to get Mac Address

def getMacAddr(f):
	print hex(ord(f.read(1))).lstrip('0x').rjust(2,'0'),
	for i in range(1,6):
		print ":",hex(ord(f.read(1))).lstrip('0x').rjust(2,'0'),

#Function to get IPv4 Address

def getIpv4Addr(f):
	print ord(f.read(1)),
	for i in range(3):
		print ":",ord(f.read(1)),
