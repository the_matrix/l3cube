import basic_byte_op
import datetime

#Function extract Pcap Header

def getPcapHeader(f):
	magic_no =   basic_byte_op.getnflip32(f)
	major_ver=   int(basic_byte_op.getnflip16(f),16)
	minor_ver=   int(basic_byte_op.getnflip16(f),16)
	zone=        int(basic_byte_op.getnflip32(f),16)
	sig_flag=    int(basic_byte_op.getnflip32(f),16)
	snap_length= int(basic_byte_op.getnflip32(f),16)
	network=     int(basic_byte_op.getnflip32(f),16)

	if magic_no!='0xa1b2c3d4':
		print "Incorrect File type"
		return 1

	if network!=1:
		print "Only Ethernet packets"
		return 1

	print "Magic Number : "               ,magic_no
	print "Major Version : "              ,major_ver
	print "Minor Version : "              ,minor_ver
	print "Timezone : "                   ,zone
	print "Significant Figures : "        ,sig_flag
	print "Snapshot Length (in bytes) : " ,snap_length
	print "Header type : "                ,network
	return 0

#Function to extract packet header

def getPacketHeader(f):
		ts_sec  = int(basic_byte_op.getnflip32(f),16)
		ts_usec = int(basic_byte_op.getnflip32(f),16)
		incl_len= int(basic_byte_op.getnflip32(f),16)
		orig_len= int(basic_byte_op.getnflip32(f),16)
		print "Timestamp:",datetime.datetime.fromtimestamp((ts_sec*1000+ts_usec)/1e3)
		print "Length of packet on network:",orig_len,"bytes"
		print "Length of packet actually captured:", incl_len,"bytes"
		return incl_len

if __name__ == "__main__":
	f=open("./arp-storm.pcap",'r')
	getPcapHeader(f)