import basic_byte_op
import get_address
import decode_tcp

#Function to decode a IPv4 packet

def decode_ipv4(f):

	#Read header contents
	
	#Read first byte
	x = ord(f.read(1))
	#Version = First 4 bits
	version = x >> 4
	#Header length = Last 4 bits * 4
	header_length = (x & 0x0f)*4
	
	#Read type of service
	tos = ord(f.read(1))
	#DSC = First 6 bits of tos
	diff_service_code = tos >> 2
	#ECN = last 2 bits of tos
	ecn = tos & 0x3

	length = int(basic_byte_op.get16(f),16)
	identification = int(basic_byte_op.get16(f),16)
	
	#Read one byte	
	y =int(basic_byte_op.get16(f),16)
	#Flag = First 3 bits
	flags = y >> 13
	#Fragment offset = Last 13 bits
	frag_offset= y << 3 >> 3

	ttl = ord(f.read(1))
	protocol = ord(f.read(1))
	checksum = basic_byte_op.get16(f).lstrip('0x')

	#Displaying contents read
	bytes_read = 20
	print "IPv:",version
	print "Header Length:",header_length ,"bytes"
	print "Differentiated Services Code Point:",diff_service_code
	print "Explicit Congestion Notification:", ecn
	print "Total Length:", length ,"bytes"
	print "Identification:", identification
	print "Flags:",flags
	print "Fragment offset:",frag_offset
	print "TTL:",ttl
	print "Protocol:", protocol
	print "Checksum:", checksum 
	print "Source Address: ",
	get_address.getIpv4Addr(f);
	print
	print "Desination Address: ", 
	get_address.getIpv4Addr(f)
	print

	#Check if TCP packet
	if protocol == 6:
		print "TCP : "
		bytes_read_by_tcp = decode_tcp.decode_tcp(f)
		bytes_read = bytes_read + bytes_read_by_tcp
	else:
		#Other protocols
		print "Protocol No. : ",protocol
	
	return bytes_read