import basic_byte_op
import get_address

#Function to decode an arp packet

def decode_arp(f):

	#Read ARP contents
	htype=int(basic_byte_op.get16(f),16)
	ptype=basic_byte_op.get16(f).lstrip('0x').rjust(4,'0')
	hlen = ord(f.read(1))
	plen = ord(f.read(1))
	oper = int (basic_byte_op.get16(f),16)
	
	#8 bytes of data read
	bytes_read = 8
	
	#Displaying contents read
	print "ARP: "
	print "Hardware Type:",htype 
	print "Protocol Type:",ptype
	print "Hardware Length:",hlen
	print "Protocol Length:",plen
	print "Operation:",

	if oper == 1 :
		print "Request"
	else:
		print "Response"

	if htype == 1:
		print "Sender Hardware Address :",
		get_address.getMacAddr(f)
		#6 bytes Mac Address read
		bytes_read = bytes_read + 6
		print
	
	if ptype == '0800':
		print "Sender Protocol Address :",
		get_address.getIpv4Addr(f)
		#4 bytes IPv4 Address read
		bytes_read = bytes_read + 4
		print
	
	if htype == 1:
		print "Target Hardware Address :",
		get_address.getMacAddr(f)
		#6 bytes Mac Address read
		bytes_read = bytes_read + 6
		print
	
	if ptype == '0800':
		print "Target Protocol Address :",
		get_address.getIpv4Addr(f)
		#4 bytes IPv4 Address read
		bytes_read = bytes_read + 4
		print
	
	return bytes_read