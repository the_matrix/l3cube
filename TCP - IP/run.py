import decodepcap
import argparse

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('pcap_file',nargs=1,help="path of the pcap file to be analyzed.")
	args = parser.parse_args()
	if args.pcap_file is None:
		print "Error. No pcap file specified."
		parser.print_help()
		sys.exit(1)
	f = open(args.pcap_file[0],'r')
	decodepcap.decode_pcap(f)