import basic_byte_op
import get_header
import get_length_type
import get_address
import decode_arp
import decode_ipv4

#Function to decode pcap packets

def decode_pcap(f):
	#Read Pcap header
	if get_header.getPcapHeader(f) != 0:
		return 1
	count=1
	while 1:
		#Read a packet
		print
		print
		print "Packet:",count
		leng = get_header.getPacketHeader(f)

		print "Destination: ",
		get_address.getMacAddr(f);
		print

		print "Source: ",
		get_address.getMacAddr(f)
		print

		#Get Ether type
		l = get_length_type.get_length_type(f);
		if ( l == '0800' ):
			print "Protocol : IPv4"
			val = decode_ipv4.decode_ipv4(f)
		elif ( l == '0806' ):
			val = decode_arp.decode_arp(f)
		else:
			print "Unsupported packet type."
			return 1
		leng = leng - 14 
		leng = leng - val
		if ( leng < 0 ):
			print "Length mismatch"
			return 1
		for j in range(leng):
			f.read(1)
		count = count + 1
		pos=f.tell()
		if f.read(1)=='':
			break
		else:
			f.seek(pos)
	return 0
