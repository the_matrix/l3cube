#Functions  for basic operations to read from a file
#get16 - reads 2 bytes of data
#get32 - reads 4 bytes of data
#getnflip16 - reads 2 bytes of data in little endian format
#getnflip32 - reads 4 bytes of data in little endian format


def get16(f):
	x1 = f.read(1)
	x2 = f.read(1)
	return hex(ord(x1) * 0x100 + ord(x2))
def get32(f):
	x1 = f.read(1)
	x2 = f.read(1)
	x3 = f.read(1)
	x4 = f.read(1)
	return hex(ord(x1)*0x1000000 + ord(x2) * 0x10000 + ord(x3)*0x100 + ord(x4)).rstrip('L')	
	return x
def getnflip16(f):
	x1 = f.read(1)
	x2 = f.read(1)
	return hex(ord(x2) * 0x100 + ord(x1))
def getnflip32(f):
	x1 = f.read(1)
	x2 = f.read(1)
	x3 = f.read(1)
	x4 = f.read(1)
	return hex(ord(x4)*0x1000000 + ord(x3) * 0x10000 + ord(x2)*0x100 + ord(x1)).rstrip('L')