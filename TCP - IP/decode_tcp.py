import basic_byte_op

#Function to decode TCP packet

def decode_tcp(f):

	#Read header contents
	source_port = int(basic_byte_op.get16(f),16)
	destination_port = int(basic_byte_op.get16(f),16)
	sequence_number = int (basic_byte_op.get32(f),16)
	ack_number = int (basic_byte_op.get32(f),16)
	flags = int(basic_byte_op.get16(f),16)
	window_size = int( basic_byte_op.get16(f),16)
	checksum = int (basic_byte_op.get16(f),16)
	urgent_p = int (basic_byte_op.get16(f),16)
	data_offset = int( flags & 0xf000 )
	options_len = ( data_offset - 5 ) * 4

	print "Source Port:",source_port 
	print "Destination Port:", destination_port
	print "Sequence Number:",sequence_number
	print "ACK Number:",ack_number
	print "Flags: " 
	if ( flags & 0x20 ) != 0:
		print "URG" 
	if ( flags & 0x10 ) != 0:
		print "ACK"
	if ( flags & 0x8 ) != 0:
		print "PSH"
	if ( flags & 0x4 ) != 0:
		print "RST" 
	if ( flags & 0x2 ) != 0:
		print "SYN"
	if ( flags & 0x1 ) != 0:
		print "FIN"
	print "Window Size:",window_size
	print "Checksum:",hex(checksum).lstrip('0x')
	print "Urgent Pointer:",urgent_p
	#Bytes read = 20 bytes
	return 20