Group No : 18

Group Name : The Matrix

Assignments Done : 5

Group Members :
- Aditi Baraskar
- Akshay Arlikatti
- Shivani Naik

Programming Language : python

All programs have been built and tested to be working on python 2.7.5+ and python 3.4.1.
No external dependencies required.